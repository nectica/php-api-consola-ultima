<?php 
/* namespace models;
 *//* use database\Database;
 */
include_once '../database/Database.php';

class EventosModel {
    
    private $conn;
    private $table = "app_datafronten";
    
    public $id;
    public $id_status ;
    public $id_evento;
    public $id_legajo;
    public $id_objetivo ;
    public $status ;
    public $nombre_operador;
    public $nombre_objetivo;
    public $tipo_evento ;
    public $fecha;
    function __construct()
    {
        $database = new Database;
        $this->conn = $database->getConnection();
    }


    function read($from){
        if($this->conn){
            try {
                $now = new DateTime('now');
                $now->setTime(0,0);
                $query = "
                    SELECT *
                    FROM 
                    ".$this->table
                    ." WHERE id > ".$from ." AND Convert( DATE ,fecha )  >= '".$now->format('Y-m-d H:i:s')."'"
                    ." ORDER BY fecha DESC , id DESC  ";
                $stmt = $this->conn->prepare($query);
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_CLASS, "EventosModel") ;
            
            } catch (Exception $e) {
            
            }
        }else{
            die();
        }
    }
}