<?php

include './../database/Database.php';

class DeviceModel {
    private $conn;
    private $table = "app_datafronten";
    public $id, $id_status, $id_legajo, $nombre_operador, $id_objectivo,
    $nombre_objectivo, $tipo_evento, $status, $fecha; 

    function __construct()
    {
        $database = new Database;
        $this->conn = $database->getConnection();
    }
    public function read($from){
        if($this->conn){
            $query = "
                SELECT *
                FROM
                ".$this->table
                ." WHERE status = 0 AND id > ".$from
                ." ORDER BY fecha DESC , id DESC "
            ;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_CLASS, "DeviceModel");
        }else{
            die();
        }

    }
}