<?php

include '../models/EventosModel.php';
include '../core/Response.php';
$eventos = new EventosModel();
$status = "success";

$data = [];
$id = 0;
if (isset($_GET["id"])) {
	$id = $_GET["id"];
}


$data =  $eventos->read($id);
if($data !== null){

    $response = compact('status', 'data');
    
    
    Response::json($response);
}
