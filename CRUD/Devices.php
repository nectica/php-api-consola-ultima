<?php 
include '../models/DeviceModel.php';
include '../core/Response.php';
$devices = new DeviceModel();
$status = 'success';

$data = [];
$id = 0 ;
if(isset($_GET["id"])  ){
    $id = $_GET["id"];   
}

$data = $devices->read($id);
if ($data !== null) {
    $response = compact('status', 'data') ;
    Response::json($response);
}
